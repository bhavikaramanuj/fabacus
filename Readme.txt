# README #

This README is to guide workflow of new question form module for Fabacus.

### Functionality and Admin Section ###

* First step is to copy the code, either manually copy and paste or by running composer.json

* Following step 1 which is copy and paste would require to copy app, etc and design folders from the given zip file to root path of the project.

* For the step 2, merging using composer.json, please unzip given folder and copy composer.json file into the project where this functionality needs to be implemented.
o From command prompt, please go to your project directory where composer.json is pasted, and run �php composer.phar install�

o This will copy all files from bitbucket repo where I have uploaded code and paste to your current project

* Once all code will be copied, please refresh your cash, or flush the cache storage

### Frontend Section ###

* Now on the home page, at the end of right bar, you would be able to see new form called Ask Your Question

* It does have all mandatory fields with email validation as well, and it will submit data using Ajax

* Once data will be submitted , it will show success message in the same box for few seconds and will clear the form

### Backend Section ###

* In the admin side, there will be new menu item called 'New Form' at the end

* It will display all records of questions in grid and also allows to Edit, delete or mass deletion as per requirement.