<?php


class Fabacus_Newform_Block_Adminhtml_Newform extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{
		parent::__construct();
		$this->_controller = "adminhtml_newform";
		$this->_blockGroup = "newform";
		$this->_headerText = Mage::helper("newform")->__("Newform Manager");
		//$this->_addButtonLabel = Mage::helper("newform")->__("Add New Item");
		$this->_removeButton('add');
	}

}