<?php
	
class Fabacus_Newform_Block_Adminhtml_Newform_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "fabnewform_id";
				$this->_blockGroup = "newform";
				$this->_controller = "adminhtml_newform";
				$this->_updateButton("save", "label", Mage::helper("newform")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("newform")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("newform")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("newform_data") && Mage::registry("newform_data")->getId() ){

				    return Mage::helper("newform")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("newform_data")->getId()));

				} 
				else{

				     return Mage::helper("newform")->__("Add Item");

				}
		}
}