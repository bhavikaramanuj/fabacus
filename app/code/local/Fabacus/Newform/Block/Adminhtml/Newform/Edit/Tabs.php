<?php
class Fabacus_Newform_Block_Adminhtml_Newform_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("newform_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("newform")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("newform")->__("Item Information"),
				"title" => Mage::helper("newform")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("newform/adminhtml_newform_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
