<?php
class Fabacus_Newform_Block_Adminhtml_Newform_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("newform_form", array("legend"=>Mage::helper("newform")->__("Item information")));

						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("newform")->__("Name"),
						"name" => "name",
						));
						
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("newform")->__("Email"),
						"name" => "email",
						));
						
						$fieldset->addField("question", "text", array(
						"label" => Mage::helper("newform")->__("Question"),
						"name" => "question",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getNewformData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getNewformData());
					Mage::getSingleton("adminhtml/session")->setNewformData(null);
				} 
				elseif(Mage::registry("newform_data")) {
				    $form->setValues(Mage::registry("newform_data")->getData());
				}
				return parent::_prepareForm();
		}
}
