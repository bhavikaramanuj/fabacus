<?php

class Fabacus_Newform_Block_Adminhtml_Newform_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("newformGrid");
				$this->setDefaultSort("fabnewform_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("newform/newform")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("fabnewform_id", array(
				"header" => Mage::helper("newform")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "fabnewform_id",
				));
				
				$this->addColumn("name", array(
				"header" => Mage::helper("newform")->__("Name"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "text",
				"index" => "name",
				));
				
				$this->addColumn("email", array(
				"header" => Mage::helper("newform")->__("Email"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "text",
				"index" => "email",
				));
				
				$this->addColumn("question", array(
				"header" => Mage::helper("newform")->__("Question"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "text",
				"index" => "question",
				));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('fabnewform_id');
			$this->getMassactionBlock()->setFormFieldName('fabnewform_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_newform', array(
					 'label'=> Mage::helper('newform')->__('Remove Question(s)'),
					 'url'  => $this->getUrl('*/adminhtml_newform/massRemove'),
					 'confirm' => Mage::helper('newform')->__('Are you sure?')
				));
			return $this;
		}
			

}