<?php 

class Fabacus_Newform_IndexController extends Mage_Core_Controller_Front_Action
{
	
    public function indexAction()
    {
		$this->loadLayout();
		$this->renderLayout();
    }
	public function submitdataAction()
    {	
		$formData['name'] = $_POST['name'];
		$formData['email'] = $_POST['email'];
		$formData['question'] = $_POST['question'];
		
		$session = Mage::getSingleton('core/session');
		$status = Mage::getModel('newform/newform')->setNewFormData($formData);
		
		echo $status;
    }
}
?>