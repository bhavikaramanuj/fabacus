<?php

class Fabacus_Newform_Adminhtml_NewformController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("newform/newform")->_addBreadcrumb(Mage::helper("adminhtml")->__("Newform  Manager"),Mage::helper("adminhtml")->__("Newform Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Newform"));
			    $this->_title($this->__("Manager Newform"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Newform"));
				$this->_title($this->__("Newform"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("newform/newform")->load($id);
				if ($model->getId()) {
					Mage::register("newform_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("newform/newform");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Newform Manager"), Mage::helper("adminhtml")->__("Newform Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Newform Description"), Mage::helper("adminhtml")->__("Newform Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("newform/adminhtml_newform_edit"))->_addLeft($this->getLayout()->createBlock("newform/adminhtml_newform_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("newform")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Newform"));
		$this->_title($this->__("Newform"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("newform/newform")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("newform_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("newform/newform");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Newform Manager"), Mage::helper("adminhtml")->__("Newform Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Newform Description"), Mage::helper("adminhtml")->__("Newform Description"));


		$this->_addContent($this->getLayout()->createBlock("newform/adminhtml_newform_edit"))->_addLeft($this->getLayout()->createBlock("newform/adminhtml_newform_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{
			$post_data=$this->getRequest()->getPost();

				if ($post_data) {

					try {
						$model = Mage::getModel("newform/newform")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Newform was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setNewformData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setNewformData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("newform/newform");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('fabnewform_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("newform/newform");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
}
