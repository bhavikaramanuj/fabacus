<?php
class Fabacus_Newform_Model_Newform extends Mage_Core_Model_Abstract
{
    protected function _construct()
	{	parent::_construct();
	   $this->_init("newform/newform");

    }
	public function setNewFormData($formArray)
	{	
		$status_record = 0;
		
		$_setName = $formArray['name'];
		$_setEmail = $formArray['email'];
		$_setQuestion = $formArray['question'];
		
		$addData = array('name'=>$_setName,'email'=>$_setEmail,'question'=>$_setQuestion);
		$this->setData($addData);
		$this->save();
		if($this->getId()!='')
		{
			$status_record = 1;
		}
		return $status_record;
	}
}
?>