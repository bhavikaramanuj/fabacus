<?php
$installer = $this;
$installer->startSetup();
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('newform')};
CREATE TABLE {$this->getTable('newform')} (
`fabnewform_id` int(11) unsigned NOT NULL auto_increment,
`name` varchar(255) NOT NULL default '',
`email` varchar(255) NOT NULL default '',
`question` text NOT NULL default '',
`created_time` datetime NULL,
`update_time` datetime NULL,
PRIMARY KEY (`fabnewform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();